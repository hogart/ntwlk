import CanvasNetwalk from './lib/CanvasNetwalk';

import fnd from 'fnd/fnd';
import evt from 'fnd/fnd.evt';

class Controls {
    /**
     * @param {HTMLCanvasElement} el Canvas element on which network will be drawn
     */
    constructor (el) {
        this.timeOut = 200;

        this.el = el;
        this.netwalk = new CanvasNetwalk(10, 10, fnd('#netwalk')[0], this.pause.bind(this));

        this.paused = false;

        this.start();
    }

    start () {
        this.paused = false;
        this.el.classList.remove('paused');
        this.el.classList.add('started');
        this.netwalk.draw();
        window.setTimeout(this.tick.bind(this), this.timeOut);
    }

    restart () {
        this.netwalk.init(10, 10);
        this.netwalk.draw();
    }

    pause () {
        this.el.classList.remove('started');
        this.el.classList.add('paused');

        this.paused = !this.paused;
    }

    save () {
        window.open(
            this.netwalk.canvas.toDataURL('image/png'),
            Date.now().toString()
        );
    }

    tick () {
        if (!this.paused) {
            this.netwalk.tick();
        }

        setTimeout(this.tick.bind(this), this.timeOut);
    }

    changeSpeed (event) {
        this.timeOut = event.target.value;
    }
}

var el = fnd('.js-buttons')[0];

evt(
    el,
    {
        'click [name="start"]': 'start',
        'click [name="restart"]': 'restart',
        'click [name="pause"]': 'pause',
        'click [name="save"]': 'save',
        'input [name="slowness"]': 'changeSpeed'
    },
    new Controls(el)
);


//console.time('mem');
//var memNetwalk = new MemoryNetwalk(20, 20, () => {
//    console.timeEnd('mem');
//    memNetwalk.printDebug();
//});
