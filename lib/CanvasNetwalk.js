import Netwalk from './Netwalk';
import Vector from './Vector';
import Dir from './Dir';

export default class CanvasNetwalk extends Netwalk {
    /**
     * @param {number} width field width in cells
     * @param {number} height field height in cells
     * @param {HTMLCanvasElement} canvas canvas element for drawing
     * @param {Function} onCompletedCallback callback to be called when network generation completes
     */
    constructor (width, height, canvas, onCompletedCallback = () => {}) {
        super(width, height);

        this.canvas = canvas;
        this.context = this.canvas.getContext('2d');


        this.cellScaleHorizontal = 1.0;
        this.cellScaleVertical = 1.0;


        /**
         * Cell side/size length
         * @type {number}
         */
        this.cellSize = Math.min(
            (this.canvas.width - this.width) * this.cellScaleHorizontal / this.width,
            (this.canvas.height - this.height) * this.cellScaleVertical / this.height
        );
        this.x0 = Math.floor((this.canvas.width - this.width * this.cellSize) / 2) + 0.5;
        this.y0 = Math.floor((this.canvas.height - this.height * this.cellSize) / 2) + 0.5;

        /**
         * @type Function
         */
        this.onCompletedCallback = onCompletedCallback
    }

    _clearCanvas () {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }

    draw () {
        this._clearCanvas();

        this.drawToVisit();

        this.drawGrid();

        this.drawBranch();

        this.drawServer();
    }

    drawBranch () {
        const context = this.context;

        context.fillStyle = 'pink';

        for (let i = 0; i < this.width; ++i) {
            for (let j = 0; j < this.height; ++j) {
                const cellIndex = this.getCellIndex(Vector.create([i, j]));
                const boardCell = this.board[cellIndex];
                const x = this.x0 + i * this.cellSize;
                const y = this.y0 + j * this.cellSize;
                for (let d in Dir) {
                    if (boardCell & Dir[d].flag) {
                        const elements = Dir[d].elements;
                        context.fillRect(
                            x + this.cellSize * (Math.min(elements[0], 0) + 1) / 3,
                            y + this.cellSize * (Math.min(elements[1], 0) + 1) / 3,
                            this.cellSize * (Math.abs(elements[0]) + 1) / 3,
                            this.cellSize * (Math.abs(elements[1]) + 1) / 3
                        )
                    }
                }
                if (Netwalk.dirCount(this.neighbours[cellIndex]) == 0 && Netwalk.dirCount(this.board[cellIndex]) == 1) {
                    this.drawTerminal(x, y);
                }
            }
        }
    }

    drawGrid () {
        const context = this.context;

        context.strokeStyle = 'silver';
        context.lineWidth = '1px';

        for (let i = 0; i <= this.width; ++i) {
            context.moveTo(this.x0 + i * this.cellSize, this.y0);
            context.lineTo(this.x0 + i * this.cellSize, this.y0 + this.height * this.cellSize);
            context.stroke();
        }
        for (let j = 0; j <= this.height; ++j) {
            context.moveTo(this.x0, this.y0 + j * this.cellSize);
            context.lineTo(this.x0 + this.width * this.cellSize, this.y0 + j * this.cellSize);
            context.stroke();
        }
    }

    drawServer () {
        this.context.fillStyle = 'navy';

        let x = this.x0 + this.serverTop.elements[0] * this.cellSize;
        let y = this.y0 + this.serverTop.elements[1] * this.cellSize;
        this.context.fillRect(
            x + this.cellSize / 8,
            y + this.cellSize / 8,
            6 * this.cellSize / 8,
            6 * this.cellSize / 8
        );
    }

    drawToVisit () {
        this.context.fillStyle = 'olive';
        for (let i = 0; i < this.toVisit.length; ++i) {
            const cell = this.toVisit[i];
            const cellIndex = this.getCellIndex(cell);
            if (Netwalk.dirCount(this.neighbours[cellIndex]) > 0) {
                let x = this.toVisit[i].elements[0];
                let y = this.toVisit[i].elements[1];
                this.context.fillRect(
                    this.x0 + x * this.cellSize,
                    this.y0 + y * this.cellSize,
                    this.cellSize,
                    this.cellSize
                )
            }
        }
    }

    drawTerminal (x, y) {
        this.context.fillStyle = 'pink';
        this.context.fillRect(
            x + this.cellSize / 4,
            y + this.cellSize / 4,
            this.cellSize / 2,
            this.cellSize / 2
        );
    }

    tick () {
        this.draw();

        super.tick();
    }

    onComplete () {
        console.log(this.printDebug());
        this.onCompletedCallback(this);
    }
}