export default class Vector {
    setElements (a) {
        this.elements = (a.elements || a).slice();
        return this
    }

    add (a) {
        const otherElements = a.elements || a;
        if (this.elements.length != otherElements.length) {
            return null
        }
        return this.map((x, i) => {
            return x + otherElements[i - 1]
        });
    }

    map (a) {
        const b = [];
        this.each((x, i) => {
            b.push(a(x, i))
        });
        return Vector.create(b)
    }

    each (a) {
        let n = this.elements.length;
        const k = n;
        let i;
        do {
            i = k - n;
            a(this.elements[i], i + 1);
        } while (--n);
    }

    static Random (n) {
        const a = [];
        do {
            a.push(Math.random());
        } while (--n);
        return Vector.create(a);
    }

    static Zero (n) {
        const a = [];
        do {
            a.push(0);
        } while (--n);
        return Vector.create(a);
    }

    static create (a) {
        const v = new Vector();
        return v.setElements(a);
    }
}
/*
function Vector() {}
Vector.prototype = {
    e: function(i) {
        return (i < 1 || i > this.elements.length) ? null : this.elements[i - 1]
    },
    dimensions: function() {
        return this.elements.length
    },
    modulus: function() {
        return Math.sqrt(this.dot(this))
    },
    eql: function(a) {
        var n = this.elements.length;
        var V = a.elements || a;
        if (n != V.length) {
            return false
        }
        do {
            if (Math.abs(this.elements[n - 1] - V[n - 1]) > Sylvester.precision) {
                return false
            }
        } while (--n);
        return true
    },
    dup: function() {
        return Vector.create(this.elements)
    },
    map: function(a) {
        var b = [];
        this.each(function(x, i) {
            b.push(a(x, i))
        });
        return Vector.create(b)
    },
    each: function(a) {
        var n = this.elements.length,
            k = n,
            i;
        do {
            i = k - n;
            a(this.elements[i], i + 1)
        } while (--n)
    },
    toUnitVector: function() {
        var r = this.modulus();
        if (r === 0) {
            return this.dup()
        }
        return this.map(function(x) {
            return x / r
        })
    },
    angleFrom: function(a) {
        var V = a.elements || a;
        var n = this.elements.length,
            k = n,
            i;
        if (n != V.length) {
            return null
        }
        var b = 0,
            mod1 = 0,
            mod2 = 0;
        this.each(function(x, i) {
            b += x * V[i - 1];
            mod1 += x * x;
            mod2 += V[i - 1] * V[i - 1]
        });
        mod1 = Math.sqrt(mod1);
        mod2 = Math.sqrt(mod2);
        if (mod1 * mod2 === 0) {
            return null
        }
        var c = b / (mod1 * mod2);
        if (c < -1) {
            c = -1
        }
        if (c > 1) {
            c = 1
        }
        return Math.acos(c)
    },
    isParallelTo: function(a) {
        var b = this.angleFrom(a);
        return (b === null) ? null : (b <= Sylvester.precision)
    },
    isAntiparallelTo: function(a) {
        var b = this.angleFrom(a);
        return (b === null) ? null : (Math.abs(b - Math.PI) <= Sylvester.precision)
    },
    isPerpendicularTo: function(a) {
        var b = this.dot(a);
        return (b === null) ? null : (Math.abs(b) <= Sylvester.precision)
    },
    add: function(a) {
        var V = a.elements || a;
        if (this.elements.length != V.length) {
            return null
        }
        return this.map(function(x, i) {
            return x + V[i - 1]
        })
    },
    subtract: function(a) {
        var V = a.elements || a;
        if (this.elements.length != V.length) {
            return null
        }
        return this.map(function(x, i) {
            return x - V[i - 1]
        })
    },
    multiply: function(k) {
        return this.map(function(x) {
            return x * k
        })
    },
    x: function(k) {
        return this.multiply(k)
    },
    dot: function(a) {
        var V = a.elements || a;
        var i, product = 0,
            n = this.elements.length;
        if (n != V.length) {
            return null
        }
        do {
            product += this.elements[n - 1] * V[n - 1]
        } while (--n);
        return product
    },
    cross: function(a) {
        var B = a.elements || a;
        if (this.elements.length != 3 || B.length != 3) {
            return null
        }
        var A = this.elements;
        return Vector.create([(A[1] * B[2]) - (A[2] * B[1]), (A[2] * B[0]) - (A[0] * B[2]), (A[0] * B[1]) - (A[1] * B[0])])
    },
    max: function() {
        var m = 0,
            n = this.elements.length,
            k = n,
            i;
        do {
            i = k - n;
            if (Math.abs(this.elements[i]) > Math.abs(m)) {
                m = this.elements[i]
            }
        } while (--n);
        return m
    },
    indexOf: function(x) {
        var a = null,
            n = this.elements.length,
            k = n,
            i;
        do {
            i = k - n;
            if (a === null && this.elements[i] == x) {
                a = i + 1
            }
        } while (--n);
        return a
    },
    toDiagonalMatrix: function() {
        return Matrix.Diagonal(this.elements)
    },
    round: function() {
        return this.map(function(x) {
            return Math.round(x)
        })
    },
    snapTo: function(x) {
        return this.map(function(y) {
            return (Math.abs(y - x) <= Sylvester.precision) ? x : y
        })
    },
    distanceFrom: function(a) {
        if (a.anchor) {
            return a.distanceFrom(this)
        }
        var V = a.elements || a;
        if (V.length != this.elements.length) {
            return null
        }
        var b = 0,
            part;
        this.each(function(x, i) {
            part = x - V[i - 1];
            b += part * part
        });
        return Math.sqrt(b)
    },
    liesOn: function(a) {
        return a.contains(this)
    },
    liesIn: function(a) {
        return a.contains(this)
    },
    rotate: function(t, a) {
        var V, R, x, y, z;
        switch (this.elements.length) {
            case 2:
                V = a.elements || a;
                if (V.length != 2) {
                    return null
                }
                R = Matrix.Rotation(t).elements;
                x = this.elements[0] - V[0];
                y = this.elements[1] - V[1];
                return Vector.create([V[0] + R[0][0] * x + R[0][1] * y, V[1] + R[1][0] * x + R[1][1] * y]);
                break;
            case 3:
                if (!a.direction) {
                    return null
                }
                var C = a.pointClosestTo(this).elements;
                R = Matrix.Rotation(t, a.direction).elements;
                x = this.elements[0] - C[0];
                y = this.elements[1] - C[1];
                z = this.elements[2] - C[2];
                return Vector.create([C[0] + R[0][0] * x + R[0][1] * y + R[0][2] * z, C[1] + R[1][0] * x + R[1][1] * y + R[1][2] * z, C[2] + R[2][0] * x + R[2][1] * y + R[2][2] * z]);
                break;
            default:
                return null
        }
    },
    reflectionIn: function(a) {
        if (a.anchor) {
            var P = this.elements.slice();
            var C = a.pointClosestTo(P).elements;
            return Vector.create([C[0] + (C[0] - P[0]), C[1] + (C[1] - P[1]), C[2] + (C[2] - (P[2] || 0))])
        } else {
            var Q = a.elements || a;
            if (this.elements.length != Q.length) {
                return null
            }
            return this.map(function(x, i) {
                return Q[i - 1] + (Q[i - 1] - x)
            })
        }
    },
    to3D: function() {
        var V = this.dup();
        switch (V.elements.length) {
            case 3:
                break;
            case 2:
                V.elements.push(0);
                break;
            default:
                return null
        }
        return V
    },
    inspect: function() {
        return '[' + this.elements.join(', ') + ']'
    },
    setElements: function(a) {
        this.elements = (a.elements || a).slice();
        return this
    }
};
Vector.create = function(a) {
    var V = new Vector();
    return V.setElements(a);
};
Vector.i = Vector.create([1, 0, 0]);
Vector.j = Vector.create([0, 1, 0]);
Vector.k = Vector.create([0, 0, 1]);
Vector.Random = function(n) {
    var a = [];
    do {
        a.push(Math.random())
    } while (--n);
    return Vector.create(a)
};
Vector.Zero = function(n) {
    var a = [];
    do {
        a.push(0)
    } while (--n);
    return Vector.create(a)
};*/