import Netwalk from './Netwalk';

export default class MemoryNetwalk extends Netwalk {
    constructor (width, height, onCompletedCallback = () => {}) {
        super(width, height);

        this.onCompletedCallback = onCompletedCallback;

        this.tick();
    }

    tick () {
        if (!this.completed) {
            super.tick();
        }

        setTimeout(this.tick.bind(this), 0);
    }

    onComplete () {
        this.completed = true;
        this.onCompletedCallback(this);
    }
}