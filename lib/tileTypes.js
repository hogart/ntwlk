const tileTypes = {
    1: '▀', // top terminal
    2: '▌', // right terminal
    3: '┘',
    4: '▐', // left terminal
    5: '└',
    6: '─',
    7: '┴',
    8: '▄', // bottom terminal
    9: '│',
    10: '┐',
    11: '┤',
    12: '┌',
    13: '├',
    14: '┬',
    15: '┼',
    17: '╻', // 1-port server (bottom)
    18: '╸', // 1-port server (left)
    19: '┛', // 2-port (left and up)
    20: '╺', // 1-port server (right)
    21: '┗', // 2-port (right and up)
    22: '━', // 2-port server (horizontal)
    23: '┻', // 3-port server (w/o bottom)
    24: '╹', // 1-port server (top)
    25: '┃', // 2-port server (vertical)
    26: '┓', // 2-port (left and down)
    27: '┫', // 3-port-server (w/o right)
    28: '┏', // 2-port (right and down)
    29: '┣', // 3-port-server (w/o left)
    30: '┳', // 3-port-server (w/o top)
    31: '╋', // 4-port server
};

export default tileTypes;