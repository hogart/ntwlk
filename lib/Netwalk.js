import random from './random';
import Vector from './Vector';
import Dir from './Dir';
import tileTypes from './tileTypes';

export default class Netwalk {
    constructor (width, height) {
        this.init(width, height);
    }

    init (width, height) {
        this.width = width;
        this.height = height;

        /**
         * @type Array.<Number>
         */
        this.board = new Array(this.width * this.height);

        /**
         * @type Array.<Number>
         */
        this.neighbours = new Array(this.width * this.height);

        /**
         * List of cell indexes to visit
         * @type Array.<Vector>
         */
        this.toVisit = [];

        for (let i = 0; i < this.width; ++i) {
            for (let j = 0; j < this.height; ++j) {
                let cellVector = Vector.create([i, j]);
                let cellIndex = this.getCellIndex(cellVector);
                this.board[cellIndex] = 0;
                this.neighbours[cellIndex] = 0;
                for (let d in Dir) {
                    let checkCell = cellVector.add(Dir[d]);
                    if (this.inBounds(checkCell)) {
                        this.neighbours[cellIndex] |= Dir[d].flag;
                    }
                }
            }
        }

        this.createServer();
    }


    createServer () {
        /**
         * @type Vector
         */
        this.serverTop = Vector.create([1 + random(this.width - 2), 1 + random(this.height - 3)]);
        this.visit(this.serverTop, 16);
    }

    static dirCount (n) {
        let count = 0;
        for (let d in Dir) {
            if (n & Dir[d].flag) {
                ++count;
            }
        }
        return count;
    }

    /**
     *
     * @param {Number} c
     * @returns {Vector}
     */
    randomFreeDir (c) {
        const n = this.neighbours[c];
        let i = random(Netwalk.dirCount(n));
        for (let d in Dir) {
            if (n & Dir[d].flag && i-- === 0) {
                return Dir[d];
            }
        }
        return null
    }

    /**
     * Get cell index by it's vector
     * @param {Vector} v
     * @returns {number}
     */
    getCellIndex (v) {
        return v.elements[0] + v.elements[1] * this.width;
    }

    /**
     * Check if given vector is in game field bounds
     * @param {Vector} v
     * @returns {boolean}
     */
    inBounds (v) {
        const x = v.elements[0];
        const y = v.elements[1];
        return x >= 0 && y >= 0 && x < this.width && y < this.height;
    }

    /**
     *
     * @param {Vector} cell
     * @param {Number} value
     */
    visit (cell, value) {
        const cellIndex = this.getCellIndex(cell);
        if (this.board[cellIndex] === 0) {
            // Never visited: add to visit list.
            this.toVisit.push(cell)
        }
        this.board[cellIndex] |= value;
        for (let d in Dir) {
            let n = cell.add(Dir[d]);
            if (this.inBounds(n)) {
                this.neighbours[this.getCellIndex(n)] &= ~Dir[d].opposite;
            }
        }
    }

    tick () {
        if (this.toVisit.length === 0) {
            this.onComplete()
        }

        let madeConnection = false;

        while (!madeConnection && this.toVisit.length > 0) {
            // Pick a random cell to visit.
            const randomIndex = random(this.toVisit.length);
            const cell = this.toVisit[randomIndex];
            const cellIndex = this.getCellIndex(cell);

            // Pick a random free direction, if one exists.
            const d = this.randomFreeDir(cellIndex);
            if (d) {
                // Add a connection in that direction.
                this.visit(cell, d.flag);
                this.visit(cell.add(d), d.opposite);
                madeConnection = true;
            }

            // Remove cell from to-visit list if all neighbours filled.
            if ((this.neighbours[cellIndex] & 15) === 0) {
                this.toVisit.splice(randomIndex, 1);
            }
        }

        if (this.debug) {
            console.log(this.printDebug());
        }
    }

    printDebug () {
        let field = '';
        for (let i = 0; i < this.width; ++i) {
            for (let j = 0; j < this.height; ++j) {
                const cellIndex = this.getCellIndex(Vector.create([j, i]));
                field += tileTypes[this.board[cellIndex]];
            }
            field += '\n';
        }

        return field;
    }

    onComplete () {
        throw new Error('onComplete not implemented');
    }
}
