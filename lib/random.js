// Return a random number in the range [0, n).
export default function random (n) {
    return Math.floor(Math.random() * n);
}