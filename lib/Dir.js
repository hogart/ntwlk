import Vector from './Vector';

const directions = ['up', 'left', 'right', 'down'];

const Dir = {
    up: Vector.create([0, -1]),
    left: Vector.create([-1, 0]),
    right: Vector.create([1, 0]),
    down: Vector.create([0, 1]),

    /*[Symbol.iterator] () {
        let index = 0;
        return {
            next: () => {
                index++;
                if (index < directions.length) {
                    return {
                        done: false, value: this[directions[index - 1]]
                    }
                } else {
                    return {
                        done: true, value: undefined
                    }
                }
            }
        }
    }*/
};

directions.forEach((directionName, index) => {
    Dir[directionName].flag = 1 << index;
    Dir[directionName].opposite = 1 << (3 - index);
});

export default Dir;